/* ChgVar.cpp: Changes of variables

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#include "ChgVar.h"
#include "tol/tol_bmfstpro.h"


//--------------------------------------------------------------------
bool ChgVarInsideHypersphere(const DMat& x, DMat& Y)
//--------------------------------------------------------------------
{
  int m = x.Rows();
  int n = x.Columns();
  int i, j, k;
  Y.Alloc(m,n);
  double* y = Y.GetData().GetBuffer();
  for(i=0; i<m; i++)
  {
    double z;
    double r = x(i,0);
    const double* f0 = x.Data().Buffer() + (i*n + 1);
    for(j=0; j<n; j++, y++)
    {
      z = r;
      const double* f = f0;
      for(k=0; k<=j-1; k++, f++)
      {
        z *= sin(*f);
      }
      if(j<n-1) { z *= cos(*f); }
      *y = z;
    }
  }
  return(true);
}  

//--------------------------------------------------------------------
bool ChgVarSortedBounded(
  const DMat& x,
  double p,
  double lower,
  double upper, 
  DMat& lambda)
//--------------------------------------------------------------------
{
  double range = upper-lower;
  bool ok = true;
  if(lower>=upper)
  {
    Error(BText("[ChgVar.SortedBounded] Argument 'lower=")+lower+
      "' should be lesser than 'upper="+upper+"'");
    ok = false;
  }
  if((p!=1)&(p!=2)&(p!=-1)&(p!=-2))
  {
    Error(BText("[ChgVar.SortedBounded] Argument 'power=")+p+
      "' should be one of these four values: 1,-1,2,-2");
    ok = false;
  }
  if(!ok) { return(false); }
  int m = x.Rows();
  int n = x.Columns();
  int i, j, k;
  lambda.Alloc(m,n);
  for(i=0; i<m; i++)
  {
    double r = x(i,0);
    const double* f0 = x.Data().Buffer() + (i*n + 1);
    double z,d=0,b;
    double* l = lambda.GetData().GetBuffer() + i*n;
    if(p<0) { l = l + (n-1); }
    for(j=0; j<n; j++)
    {
      z = r;
      const double* f = f0;
      for(k=0; k<=j-1; k++, f++)
      {
        z *= sin(*f);
      }
      if(j<n-1) { z *= cos(*f); }
      d += z*z;
      b = lower + d * range;
      if(p==2) 
      { 
        b = sqrt(b); 
        if(z<0) { b *= -1; }
      }
      else if(p==-2) 
      { 
        b = 1/sqrt(b); 
        if(z<0) { b *= -1; }
      }
      else if(p==-1) 
      { 
        b = 1/b;
      }
      *l = b;
      if(p>=0) { l++; }    
      else     { l--; }
    }
  }
  return(true);
}  

//--------------------------------------------------------------------
bool ChgVarCorrelation(const DMat& X, DMat& W)
//--------------------------------------------------------------------
{
  int m = X.Rows();
  int n = X.Columns();
  double k_ = (1.0+sqrt(1.0+8.0*n))/2.0;
  int k = (int)round(k_);
  bool ok = fabs(k-k_)<1E-5;
  if(!ok)
  {
    Error(BText("[ChgVar.Correlation] Number of columns in 'x' (")+n+
      ") should be a piramidal number k*(k-1)/2");
    return(false);
  }
  int f,g, h, i, j;
  W.Alloc(m,k*(k+1)/2);
  double* w  = W.GetData().GetBuffer();
  const double* x0 = X.Data().Buffer();
  for(h=0; h<m; h++)
  {
    *w = 1;
    w++;
    for(f=i=1; i<k; i++)
    {
      for(j=0; j<=i; j++, f++, w++)
      {
        const double* x = x0 + h*n + i*(i-1)/2;
        *w = 1;
        for(g=0; g<j; g++, x++)
        {
          *w *= sin(*x);
        }
        if(j<i)
        {
          *w *= cos(*x);
        }
      }
    }
  }
  return(true);
}  


//--------------------------------------------------------------------
bool ChgVarRotation(const DMat& X, DMat& R)
//--------------------------------------------------------------------
{
  int m = X.Rows();
  int n = X.Columns();
  double k_ = (1.0+sqrt(1.0+8.0*n))/2.0;
  int k = (int)round(k_);
  bool ok = fabs(k-k_)<1E-5;
  if(!ok)
  {
    Error(BText("[ChgVar.Rotation] Number of columns in 'x' (")+n+
      ") should be a piramidal number k*(k-1)/2");
    return(false);
  }
  int k2 = k*k;
  int k2d = sizeof(double)*k2;
  int g, h, i, j;
  R.Alloc(m,k*k);
  double* r = R.GetData().GetBuffer();
  const double* x = X.Data().Buffer();
  for(h=0; h<m; h++, r+=k2)
  {
    BVMat RH;
    RH.Eye(k); 
    for(i=0; i<k; i++)
    {
      for(j=i+1; j<k; j++, x++)
      {
        BVMat RijT, RijS;
        DMat Rij(k+2,3);
        double c = cos(*x);
        double s = sin(*x);
        for(g=0;g<k;g++)
        {
          Rij(g,0)=g+1;
          Rij(g,1)=g+1;
          Rij(g,2)=((g==i)||(g==j))?c:1;
        }
        Rij(k,  0)=i+1;
        Rij(k,  1)=j+1;
        Rij(k,  2)=-s;
        Rij(k+1,0)=j+1;
        Rij(k+1,1)=i+1;
        Rij(k+1,2)=s;
        RijT.DMat2triplet(Rij,k,k);
        RijS.Convert(RijT,BVMat::ESC_chlmRsparse);
        RH = RijS*RH;
      }
    }
    DMat Rh(k,k);
    RH.GetDMat(Rh);
    memcpy (r, Rh.Data().Buffer(), k2d);
  }
  return(true);
}


//--------------------------------------------------------------------
bool ChgVarRotatedDiagDecomSquare(
  const DMat& X, bool sortedEigenvalues, DMat& C)
//--------------------------------------------------------------------
{
  int m = X.Rows();
  int n = X.Columns();
  double k_ = sqrt(1.0*n);
  int k = (int)round(k_);
  bool ok = fabs(k-k_)<1E-5;
  if(!ok)
  {
    Error(BText("[ChgVar.RotatedDiagDecom.Square] Number of columns in 'x' (")+n+
      ") should be a square integer number k^2");
    return(false);
  }
  C.Alloc(m,n);
  double* c = C.GetData().GetBuffer();
  DMat r, d, q, e, R, Q;
  int kp = k*(k-1)/2;
  int k2 = k*k;
  int k2d = sizeof(double)*k2;
  d.Alloc(m,k);
  r.Alloc(m,kp);
  q.Alloc(m,kp);
  int h;
  for(h=0; h<m; h++)
  {
    int j=0, jd=0, jr=0, jq=0; 
    for(; jd<k;  jd++, j++) { d(h,jd) = X(h,j); }
    for(; jr<kp; jr++, j++) { r(h,jr) = X(h,j); }
    for(; jq<kp; jq++, j++) { q(h,jq) = X(h,j); }
  }
  if(sortedEigenvalues)
  {
    ChgVarSortedBounded(d, 2, 0, 1, e);
  }
  else
  {
    e = d;
  }
  ChgVarRotation(r, R);
  ChgVarRotation(q, Q);  
  for(h=0; h<m; h++, c+=k2)
  {
    BVMat Dh, Rh, Qh;
    ReverseRow2DiagonalV(e, h, Dh);
    Row2SquareV  (R, h, Rh);
    Row2SquareV  (Q, h, Qh);
    BVMat Ch = Rh*Dh*Qh.T();
    DMat CH;
    Ch.GetDMat(CH);
    memcpy(c, CH.Data().Buffer(), k2d);
  }  
  return(true);
}

//--------------------------------------------------------------------
bool ChgVarStationaryPolyn(const DMat& X, DMat& S)
//--------------------------------------------------------------------
{
  int m = X.Rows();
  int n = X.Columns();
  int h,k,p;
  S.Alloc(m,n+1);
  double* s = S.GetData().GetBuffer();
  const double* x = X.Data().Buffer();
  for(h=0; h<m; h++)
  {
    BArray<double> q0(n+1);
    BArray<double> q1(n+1);
    q0.ReallocBuffer(1);
    q0(0) = 1;
    for(p=1; p<=n; p++, x++)
    {
      double c = *x;
      q1.ReallocBuffer(p+1);
      q1(0) = c;
      q1(p) = 1;
      for(k=1; k<p; k++)
      {
        q1(k) = q0(k-1) + c*q0(p-k-1);
      }
      q0 = q1;
    }
    for(k=0; k<=n; k++, s++)
    {
      *s = q0(n-k);
    }
  }
  return(true);
}

/* */

bool StableSquareSet2DuffinSchurBinFact(const DMat* C, int deg, BPolMat& P);

//--------------------------------------------------------------------
bool StableSquareSet2DuffinSchur(const DMat* C, int deg, BPolMat& P)
//--------------------------------------------------------------------
{
  if(deg==1)
  {
    const DMat& c = *C;
    int i,j,n = c.Rows();
    P.Alloc(n,n);
    for(i=0; i<n; i++)
    {
      for(j=0; j<n; j++)
      {
        BPol u = BPol::Zero();
        if(i==j) { u = BPol::One(); }
        P(i,j) = u + c(i,j)*BPol::B();
      }
    }
  }
  else if(deg==2)
  {
    const DMat& c1 = *C;
    const DMat& c2 = *(C+1);
    int i,j,n = c1.Rows();
    DMat I(n,n);
    I.SetAllValuesTo(0.0);
    for(i=0; i<n; i++) { I(i,i)=1; }
    DMat a1 = (I+c2)*c1;
    const DMat& a2 = c2;
    P.Alloc(n,n);
    BPol Z = BPol::Zero();
    BPol U = BPol::One();
    BPol B = BPol::B();
    BPol B2 = B^2;
    for(i=0; i<n; i++)
    {
      for(j=0; j<n; j++)
      {
        BPol u = Z;
        if(i==j) { u = U; }
        P(i,j) = u + a1(i,j)*B + a2(i,j)*B2;
      }
    }
  }
  else
  {
    return(StableSquareSet2DuffinSchurBinFact(C, deg, P));
  }
  return(true);
}

//--------------------------------------------------------------------
bool StableSquareSet2DuffinSchurBinFact(const DMat* C, int deg, BPolMat& P)
//--------------------------------------------------------------------
{
  if(deg<=2) { StableSquareSet2DuffinSchur(C,deg,P); }
  else
  {
    BPolMat P1, P2;
    StableSquareSet2DuffinSchur(C,      2,P1);
    StableSquareSet2DuffinSchur(C+2,deg-2,P2);
    MatMult(P1,P2,P);
  }
  return(true);
}

//--------------------------------------------------------------------
bool StableSquareSet2DuffinSchur(const BArray<DMat>& C,BPolMat& P)
//--------------------------------------------------------------------
{
  return(StableSquareSet2DuffinSchur(C.Buffer(),C.Size(),P));
}

//--------------------------------------------------------------------
bool StableSquareSet2DuffinSchurBinFact(const BArray<DMat>& C,BPolMat& P)
//--------------------------------------------------------------------
{
  return(StableSquareSet2DuffinSchurBinFact(C.Buffer(),C.Size(),P));
}

//--------------------------------------------------------------------
bool ChgVarStationaryPolMat(
  const DMat& X, 
  int dim, 
  int degree, 
  BPolMat& P)
//--------------------------------------------------------------------
{
  int s = X.Data().Size();
  int dim2 = dim*dim;
  if(s!=degree*dim2)
  {
    Error(BText("[ChgVar.ChgVarStationaryPolMat] Invalid dimensions"));
    return(false);   
  }
  int d,h,j;
  P.Alloc(dim);
  BArray< DMat > C (degree);
  const double* xp = X.Data().Buffer();
  for(d=h=0; d<degree; d++, h+=dim2)
  {
    DMat& c = C(d);
  //DMat x = X.Sub(0, h, 1, dim2);
    DMat y, x(1,dim2);
    for(j=0; j<dim2; j++, *xp++) { x(0,j) = *xp; }
    ChgVarRotatedDiagDecomSquare(x,false,y);
    Row2Square(y, 0, c);
  }
  StableSquareSet2DuffinSchurBinFact(C,P);
  return(true);
}

//--------------------------------------------------------------------
bool ChgVarStationaryPolMat2Vector(
  const BPolMat& P, 
  int degree, 
  DMat& X)
//--------------------------------------------------------------------
{
  int dim = P.Rows();
  int dim2 = dim*dim;
  int i,j,k,d,s;
  s = dim2*degree;
  X.Alloc(1,s);
  double* x = X.GetData().GetBuffer();
  for(i=k=0; i<dim; i++)
  {
    for(j=0; j<dim; j++)
    {
      const BPol& p = P(i,j);
      for(d=1; d<=degree; d++, x++)
      {
        *x = p.Coef(d).Value();
      }
    }
  } 
  return(true);
}


//--------------------------------------------------------------------
bool ChgVarStationaryVector2PolMat(const DMat& X, int degree, BPolMat& P)
//--------------------------------------------------------------------
{
  int s = X.Data().Size();
  double dim_ = sqrt((double)s/(double)degree);
  int dim = round(dim_);
  int dim2 = dim*dim;
  int s_ = dim2*degree;
  if(s!=s_)
  {
    Error(BText("[ChgVarStationaryVector2PolMat] Invalid dimensions"));
    return(false);
  }
  int i,j,k,d;
  P.Alloc(dim,dim);
  const double* x = X.Data().Buffer();
  for(i=k=0; i<dim; i++)
  {
    for(j=0; j<dim; j++)
    {
      BPol& p = P(i,j);
      p.AllocBuffer(degree+1);
      p(0).PutCoef(i==j);
      p(0).PutDegree(0);
      for(d=1; d<=degree; d++, x++)
      {
        p(d).PutDegree(d);
        p(d).PutCoef(*x);
      }
      p.Aggregate();
    }
  } 
  return(true);
}


//--------------------------------------------------------------------
bool ChgVarStationaryPolMat2Vector(
  const BPolMat& P, 
  const BPolMat& mask, 
  DMat& X)
//--------------------------------------------------------------------
{
  int dim = P.Rows();
  bool ok = true;
  if(dim!=P.Columns())
  {
    Error(BText("[ChgVarStationaryVector2PolMat] Argument P must be square"));
    ok = false;
  }
  if((dim!=mask.Rows()) || (dim!=mask.Columns()))
  {
    Error(BText("[ChgVarStationaryVector2PolMat] Argument mask must have same dimensions than P"));
    ok = false;
  }
  if(!ok) { return(false); }
  int dim2 = dim*dim;
  int i,j,k,s;
  s = 0;
  for(i=0; i<dim; i++)
  {
    for(j=0; j<dim; j++)
    {
      const BPol& p = mask(i,j);
      const BMonome<BDat>* m = p.Buffer();
      for(k=0; k<p.Size(); k++, m++)
      {
        if(m->Degree())
        {
          s++;
        }
      }
    }
  }
  X.Alloc(1,s);
  double* x = X.GetData().GetBuffer();
  for(i=k=0; i<dim; i++)
  {
    for(j=0; j<dim; j++)
    {
      const BPol& p = P(i,j);
      const BPol& q = mask(i,j);
      const BMonome<BDat>* m = q.Buffer();
      for(k=0; k<p.Size(); k++,m++)
      {
        int deg = m->Degree();
        if(deg)
        {
          *x = p.Coef(deg).Value();
          x++;
        }
      }
    }
  } 
  return(true);
}


//--------------------------------------------------------------------
bool ChgVarStationaryVector2PolMat(const DMat& X, const BPolMat& mask, BPolMat& P)
//--------------------------------------------------------------------
{
  int s = X.Data().Size();
  int dim = mask.Rows();
  int dim2 = dim*dim;
  int i,j,k,h;
  P = mask;
  const double* x = X.Data().Buffer();
  for(i=h=0; i<dim; i++)
  {
    for(j=0; j<dim; j++)
    {
      BPol& p = P(i,j);
      BMonome<BDat>* m = p.GetBuffer();
      for(k=0; k<p.Size(); k++, m++)
      {
        if(h==s)
        {
          Error("[ChgVarStationaryVector2PolMat] Not enought space in argument X to store mask");
          return(false);
        }
        if(m->Degree())
        {
          m->PutCoef(*x);
          x++;
          h++;
        }
      }
    }
  } 
  if(h<s)
  {
    Error("[ChgVarStationaryVector2PolMat] Not enought coefficients in argument mask to fill X");
    return(false);
  }
  return(true);
}


/* */