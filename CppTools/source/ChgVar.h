/* ChgVar.h: Changes of variables

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */

#ifndef _DEFINE_ChgVar_
#define _DEFINE_ChgVar_

#include "Row2Mat.h"
#include <tol/tol_bpolmatgra.h>

bool ChgVarInsideHypersphere(const DMat& x, DMat& Y);
bool ChgVarSortedBounded(const DMat& x,double p,double lower,double upper,DMat& lambda);
bool ChgVarCorrelation(const DMat& X, DMat& W);
bool ChgVarRotation   (const DMat& X, DMat& R);
bool ChgVarRotatedDiagDecomSquare(const DMat& X, bool sortedEigenvalues, DMat& C);
bool ChgVarStationaryPolyn(const DMat& X, DMat& S);
bool ChgVarStationaryPolMat(const DMat& X, int dim, int degree, BPolMat& P);
bool ChgVarStationaryPolMat2Vector(const BPolMat& P, int degree, DMat& X);
bool ChgVarStationaryVector2PolMat(const DMat& X, int degree, BPolMat& P);
bool ChgVarStationaryPolMat2Vector(const BPolMat& P, const BPolMat& mask, DMat& X);
bool ChgVarStationaryVector2PolMat(const DMat& X, const BPolMat& mask, BPolMat& P);
#endif
