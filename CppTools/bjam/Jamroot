# -*- mode: conf -*-

import common ;
import config ;
import modules ;
import os ;
import option ;
import package ;
import path ;
import property ;
import virtual-target ;
import type ;

#constant TOL_VERSION : 2.0.1 ;

path-constant TOP : . ;

path-constant PKG_ROOT : $(TOP)/.. ;
path-constant SOURCE : $(PKG_ROOT)/source ;
path-constant TOL_SDK_PATH  : [ os.environ TOL_SDK_PATH ] ;

path-constant GSL_INC : [ os.environ GSL_INC ] ;
path-constant GSL_LIB : [ os.environ GSL_LIB ] ;

local os ;
if [ modules.peek : UNIX ]
{ 
  os = UNIX ;
}
else 
{ 
  os ?= [ os.name ] ; 
}

if  $(os)  = NT
{
  using msvc ;
  os-reqs = <define>__WIN32__
		<define>WIN32
		<define>_WINDOWS
 		<define>_USRDLL
		<define>_WINDLL ;
  tolsuffix = ;
  gsllibname = libgsl_dll ;
  path-constant GSL_LIB : [ os.environ GSL_LIB ] ;
  GSL_LIB = $(GSL_LIB)/release ;
}
else
{
  os-reqs = <include>$(TOL_SDK_PATH)/include ;
  tolsuffix = $(TOL_VERSION) ;
  gsllibname = gsl ;
}

lib gsl : : <name>$(gsllibname) <search>$(GSL_LIB) ;


# Configure a feature hash_map which can take the following values:
#   msvc   ==>  __USE_HASH_MAP__=__HASH_MAP_NATIVE__
#   google ==>  __USE_HASH_MAP__=__HASH_MAP_GOOGLE__
#
#  the default value is google
#
configure enable : hash_map : __USE_HASH_MAP__ 
                 : google,__HASH_MAP_GOOGLE__
		   native,__HASH_MAP_NATIVE__ ;

# Configure a feature pool which can take the following values:
#   none   ==>  __USE_POOL__=__POOL_NONE__
#   bfsmen ==>  __USE_POOL__=__POOL_BFSMEM__
#
#  the default value is bfsmem
#
configure enable : pool : __USE_POOL__ 
                 : bfsmem,__POOL_BFSMEM__ none,__POOL_NONE__ ;

# Configure a feature 'zarch'  which can take the following values:
#   yes ==>  #define __USE_ZIP_ARCHIVE__
#   no  ==>  
#
#   the default value is: yes
#
configure enable : zarch : __USE_ZIP_ARCHIVE__ : yes no ;

# Configure a feature 'nameblock'  which can take the following values:
#   yes ==>  #define __USE_NAMESPACE__
#   no  ==>  
#
#   the default value is: yes
#
#configure enable : nameblock : __USE_NAMEBLOCK__ : yes no ;

# Configure a feature 'dynscope' which can take the following values:
#   yes ==>  #define __USE_DYNSCOPE__
#   no  ==>
#
#   the default value is: yes
# 
configure enable : dynscope : __USE_DYNSCOPE__ : yes no ;

#config.read [ path.join $(TOP) prj-config.jam  ] ;

project MatQuery
  : requirements <include>$(PKG_ROOT)
                 <include>$(TOL_SDK_PATH)
                 <include>$(GSL_INC)
                 $(os-reqs)
                 <define>HAVE_CONFIG_H
                 <define>USE_DELAY_INIT
                 <define>__POOL_NONE__=0
                 <define>__POOL_BFSMEM__=1
                 <define>__HASH_MAP_MSVC__=1
                 <define>__HASH_MAP_GOOGLE__=2
                 <define>__HASH_MAP_GCC__=3
                 <define>__HASH_MAP_ICC__=4
                 <define>_USE_MM_IO_
                 <toolset>gcc:<cxxflags>-fno-strict-aliasing
  ;

rule tag ( name : type ? : property-set )
{
    if $(type) = SHARED_LIB
    {
        local suffix = [ type.generated-target-suffix $(type) 
                                                      : $(property-set) ] ;
        suffix = .$(suffix) ;
        return $(name)$(suffix:E="") ;
    }
}

lib tol : : <name>tol <search>$(TOL_SDK_PATH)/lib ;

CppSources = MatQuery.cpp ;

lib MatQuery : $(SOURCE)/$(CppSources) tol gsl : <tag>@tag ;
